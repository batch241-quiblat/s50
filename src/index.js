import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// const name = "Shane Mar Quiblat";
// const element = <h1>Hello, {name}</h1>

// const user = {
//   firstName: 'Shane Mar',
//   lastName: 'Quiblat'
// }

// function formatName(user){
//   return user.firstName + ' ' + user.lastName;
// }

// const element = <h1>Hello, {formatName(user)}</h1>

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals

// root.render(element);