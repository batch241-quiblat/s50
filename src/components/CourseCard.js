import {Card, Button} from 'react-bootstrap';

export default function CouseCard() {
  return (
    <Card>
        <Card.Body>
            <Card.Title>
              <h4>Sample Course</h4>
            </Card.Title>
            <Card.Text>
              <b>Description:</b><br/>
              This is a sample course offering.
            </Card.Text>
            <Card.Text>
              <b>Price:</b><br/>
              PhP 40,000
            </Card.Text>
            <Button variant="primary">Enroll</Button>
        </Card.Body>
    </Card>
  )
}